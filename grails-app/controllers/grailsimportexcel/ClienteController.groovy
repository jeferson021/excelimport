package grailsimportexcel

import jxl.LabelCell
import jxl.Sheet
import jxl.Workbook

class ClienteController {
    private final static int COLUMN_NAME = 0
    private final static int COLUMN_SENHA = 1
    private final static int COLUMN_EMAIL = 2

    def index() {
        render (view:"/excelImporter/index")
    }

    def doUpload() {
        def count = 0
        def file = request.getFile('file')
        Workbook workbook =   Workbook.getWorkbook(file.getInputStream())
        Sheet sheet = workbook.getSheet(0)

        // skip first row (row 0) by starting from 1
        for (int row = 1; row < sheet.getRows(); row++) {

            LabelCell email = sheet.getCell(COLUMN_EMAIL, row)
            LabelCell nome = sheet.getCell(COLUMN_NAME, row)
            LabelCell senha = sheet.getCell(COLUMN_SENHA, row)

            Cliente cl = new Cliente()
            cl.nome =  nome.string
            cl.email = email.string
            cl.senha = senha.string

            cl.validate()
            if(!cl.hasErrors()) {
                cl.save(flush: true)
                count++
            }
        }

        flash.message = count+" registros importados!"
        redirect action:"index"
    }

}