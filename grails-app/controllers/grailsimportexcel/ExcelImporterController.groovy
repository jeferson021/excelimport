package grailsimportexcel


import org.apache.poi.xssf.usermodel.XSSFWorkbook
import static org.apache.poi.ss.usermodel.Cell.*

class ExcelImporterController {

    def index() {
        render (view:"/excelImporter/index")
    }

    def uploadFile() {

        def file = request.getFile('excelFile') // arquivo selecionado no form
        if(!file.empty) {
            def sheetheader = []
            def values = []
            def workbook = new XSSFWorkbook(file.getInputStream())
            def sheet = workbook.getSheetAt(0)

            // pegando os valores de cabeçalho (linha 0)
            for (cell in sheet.getRow(0).cellIterator()) {

                sheetheader << cell.stringCellValue
            }

            def headerFlag = true
            for (row in sheet.rowIterator()) {

                // Não pode ser o cabeçalho
                if (headerFlag) {
                    headerFlag = false
                    continue
                }

                def value = ''
                def map = [:]

                // percorrendo as células e adicionando os valores ao Map
                for (cell in row.cellIterator()) {

                    switch(cell.cellType) {
                        case 1:
                            value = cell.stringCellValue
                            map["${sheetheader[cell.columnIndex]}"] = value
                            break
                        case 0:
                            value = cell.numericCellValue
                            map["${sheetheader[cell.columnIndex]}"] = value
                            break
                        default:
                            value = ''
                    }
                }
                values.add(map)
            }

            values.each { v ->
                if(v) {
                  // Subscriber.findByEmail(v.email)?: new Subscriber(email:v.email,fullname:v.fullname).save(flush:true)
                  new Subscriber(email:v.email,fullname:v.fullname).save(flush:true)
                }
            }

            flash.message = "Subscriber imported successfully"
            redirect action:"index"
        }
    }




}
