package grailsimportexcel

class Cliente {

    String nome
    String email
    String senha

    // static hasMany = [pedidos:Pedido, produtosPreferidos:Produto]

    static constraints = {
        nome nullable: false, blank: false
        email  nullable: false, blank: false
        senha  nullable: false, blank: false
    }

    static mapping = {

    }
}

