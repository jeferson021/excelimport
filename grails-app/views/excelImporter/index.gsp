<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Excel Importer</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

<div id="content" role="main">

    <div id="upload-data" class="content" role="main">
        <div class="content" role="main">
            <h1>Upload XLSX Files </h1>
            <g:if test="${flash.message}">
                <div class="message" role="alert">
                    ${flash.message}
                </div>
            </g:if>
            <g:uploadForm url="[controller: 'ExcelImporter', action: 'uploadFile']">
                <fieldset>
                    <div class="fieldcontain">
                        <input type="file" name="excelFile" />
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="uploadFile"  value="Upload" />
                </fieldset>
            </g:uploadForm>
        </div>
    </div>


    <div id="upload-data" class="content" role="main">
        <div class="content" role="main">
            <h1>Upload XLS Files </h1>

            <g:if test="${flash.message}">
                <div class="message" role="alert">
                    ${flash.message}
                </div>
            </g:if>

            <g:uploadForm url="[controller: 'cliente', action: 'doUpload']" update="divLista">
                <fieldset class="form">
                    <input type="file" name="file" />
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="doUpload" value="Upload"/>
                </fieldset>
            </g:uploadForm>
        </div>
    </div>

</div>

</body>